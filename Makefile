

all:
	make clean
	pdflatex main.tex
	songidx titleidx.sxd
	pdflatex main.tex
	make clean


clean:
	rm -f *.aux *.lof *.log *.lot *.toc *.bbl *.blg *.out *.run.xml *-blx.bib *.bcf *.lol *.upa *.upb
